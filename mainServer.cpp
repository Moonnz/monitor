#include <iostream>
#include <systemd/sd-daemon.h>
#include <csignal>
#include <atomic>
#include <chrono>

#include "InterruptableSleeper.cpp"
#include "Transceiver.h"
#include "SystemMonitor.h"

std::atomic_bool running;
InterruptableSleeper sleeper;

void signal_handler(int signal) {
    running = false;
    sleeper.wake();
}

int main(int argc, char** argv) {
    std::cout << "Starting..." << std::endl;
    running = true;
    std::signal(SIGTERM, signal_handler);

    SystemMonitor sysMon;
    sysMon.getRecap();

    Transceiver transceiver(9019);
    transceiver.init();

    while (running) {
        sd_notify(0, "WATCHDOG=1");
        Request receivedRequest = transceiver.checkSocket();
        if (receivedRequest.valid) {
            if(transceiver.isValid(&receivedRequest)){
                Response res;
                res.message = sysMon.getRecap();
                res.ip = receivedRequest.ip;
                res.port = std::to_string(receivedRequest.port);
                transceiver.sendResponse(res);
            }
        }
        sleeper.sleepFor(std::chrono::milliseconds(1000));
    };

    std::cout << "Stopping..." << std::endl;

    return (EXIT_SUCCESS);
}