//
// Created by martin on 22/07/2020.
//

#include "SystemMonitor.h"

std::tuple<int, int, int> SystemMonitor::getRam() {
    int total, used, available;
    FILE* file = fopen("/proc/meminfo", "r");
    if(fscanf(file, "MemTotal: %d kB MemFree: %d kB MemAvailable: %d kB",&total, &used, &available) < 3)
    {
        total = -1;
        used = -1;
        available = -1;

    } else {
        used = total - available;
    }

    fclose(file);
    return std::make_tuple(used, available, total);
}

std::string SystemMonitor::getUptime() {
    struct sysinfo si;
    sysinfo (&si);
    char buf[256];
    int n = sprintf (buf, "%ld days, %ld:%02ld:%02ld",
                     si.uptime / day, (si.uptime % day) / hour,
                     (si.uptime % hour) / minute, si.uptime % minute);
    buf[n] = '\0';
    return std::string (buf);
}

std::vector<std::pair<std::string, std::string>> SystemMonitor::getCPUUsage() {
    std::vector<CPUData> entries1;
    std::vector<CPUData> entries2;
    std::vector<std::pair<std::string, std::string>> result;

    readStatsCPU(entries1);

    std::this_thread::sleep_for(std::chrono::milliseconds(100));

    readStatsCPU(entries2);

    const size_t NUM_ENTRIES = entries1.size();
    for(size_t i = 0; i < NUM_ENTRIES; ++i) {
        const CPUData & e1 = entries1[i];
        const CPUData & e2 = entries2[i];

        const float ACTIVE_TIME	= static_cast<float>(GetActiveTime(e2) - GetActiveTime(e1));
        const float IDLE_TIME	= static_cast<float>(GetIdleTime(e2) - GetIdleTime(e1));
        const float TOTAL_TIME	= ACTIVE_TIME + IDLE_TIME;

        result.emplace_back(std::pair<std::string, std::string>(e1.cpu, std::to_string(100.f * ACTIVE_TIME / TOTAL_TIME)));
    }
    return result;
}

int SystemMonitor::getCPUTemp() {
    std::ifstream fileStat("/proc/acpi/ibm/thermal");
    std::string line;

    std::getline(fileStat, line);

    auto forTrunc = split(line, "\t");
    auto elems = split(forTrunc[1], " ");

    return stoi(elems[0]);
}

void SystemMonitor::readStatsCPU(std::vector<CPUData> &entries) {
    std::ifstream fileStat("/proc/stat");

    std::string line;

    const std::string STR_CPU("cpu");
    const std::size_t LEN_STR_CPU = STR_CPU.size();
    const std::string STR_TOT("tot");

    while(std::getline(fileStat, line))
    {
        // cpu stats line found
        if(!line.compare(0, LEN_STR_CPU, STR_CPU))
        {
            std::istringstream ss(line);

            // store entry
            entries.emplace_back(CPUData());
            CPUData & entry = entries.back();

            // read cpu label
            ss >> entry.cpu;

            if(entry.cpu.size() > LEN_STR_CPU)
                entry.cpu.erase(0, LEN_STR_CPU);
            else
                entry.cpu = STR_TOT;

            // read times
            for(int i = 0; i < NUM_CPU_STATES; ++i)
                ss >> entry.times[i];
        }
    }
}

size_t SystemMonitor::GetIdleTime(const CPUData &e) {
    return  e.times[S_IDLE] +
            e.times[S_IOWAIT];
}

size_t SystemMonitor::GetActiveTime(const CPUData &e) {
    return  e.times[S_USER] +
            e.times[S_NICE] +
            e.times[S_SYSTEM] +
            e.times[S_IRQ] +
            e.times[S_SOFTIRQ] +
            e.times[S_STEAL] +
            e.times[S_GUEST] +
            e.times[S_GUEST_NICE];
}

std::vector<std::string> SystemMonitor::split(const std::string& str, const std::string& delim) {
    std::vector<std::string> tokens;
    size_t prev = 0, pos = 0;
    do
    {
        pos = str.find(delim, prev);
        if (pos == std::string::npos) pos = str.length();
        std::string token = str.substr(prev, pos-prev);
        if (!token.empty()) tokens.push_back(token);
        prev = pos + delim.length();
    }
    while (pos < str.length() && prev < str.length());
    return tokens;
}

int SystemMonitor::getFanSpeed() {
    std::ifstream fileStat("/proc/acpi/ibm/fan");
    std::string line;

    std::getline(fileStat, line);
    line.erase();
    std::getline(fileStat, line);

    auto elems = split(line, "\t\t");

    return elems.size() > 0 ? stoi(elems[1]) : 0;
}

std::string SystemMonitor::getRecap() {
    auto ramRecap = this->getRam();
    auto cpuUsageRecap = this->getCPUUsage();

    return std::to_string(std::get<0>(ramRecap))+"::"
    +std::to_string(std::get<1>(ramRecap))+"::"
    +std::to_string(std::get<2>(ramRecap))+"::"
    +this->getUptime()+"::"
    +std::to_string(this->getCPUTemp())+"::"
    +std::to_string(this->getFanSpeed())+"::"
    +cpuUsageRecap[0].second;
}
