//
// Created by martin on 24/07/2020.
//

#ifndef MONITOR_TYPES_H
#define MONITOR_TYPES_H

#include <string>

const int NUM_CPU_STATES = 10;

const int S_USER = 0;
const int S_NICE = 1;
const int S_SYSTEM = 2;
const int S_IDLE = 3;
const int S_IOWAIT = 4;
const int S_IRQ = 5;
const int S_SOFTIRQ = 6;
const int S_STEAL = 7;
const int S_GUEST = 8;
const int S_GUEST_NICE = 9;

const std::string CPU_STATES_LABEL[10] = {"S_USER", "S_NICE", "S_SYSTEM", "S_IDLE", "S_IOWAIT", "S_IRQ", "S_SOFTIRQ", "S_STEAL", "S_GUEST", "S_GUEST_NICE"};

typedef struct CPUData
{
    std::string cpu;
    size_t times[NUM_CPU_STATES];
} CPUData;

#endif //MONITOR_TYPES_H
