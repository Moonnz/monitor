//
// Created by martin on 17/07/2020.
//

#ifndef MONITOR_SERVER_RESPONSE_H
#define MONITOR_SERVER_RESPONSE_H

#include <string>

struct Response
{
    std::string message;
    std::string ip;
    std::string port;
};

#endif //MONITOR_SERVER_RESPONSE_H
