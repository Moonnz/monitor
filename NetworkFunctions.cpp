//
// Created by martin on 17/07/2020.
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include <iostream>

#define PORT  9019

int receiver() {
    int sockfd;
    char buffer[1024];
    struct sockaddr_in servaddr, cliaddr;

    // Creating socket file descriptor
    if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
        std::cerr << "socket creation failed" << std::endl;
        return -1;
    }

    memset(&servaddr, 0, sizeof(servaddr));
    memset(&cliaddr, 0, sizeof(cliaddr));

    servaddr.sin_family = AF_INET; // IPv4
    servaddr.sin_addr.s_addr = INADDR_ANY;
    servaddr.sin_port = htons(PORT);

    // Bind the socket with the server address
    if ( bind(sockfd, (const struct sockaddr *)&servaddr, sizeof(servaddr)) < 0 ) {
        std::cerr << "bind failed" << std::endl;
        return -1;
    }

    unsigned int len, n;

    len = sizeof(cliaddr);

    n = recvfrom(sockfd, (char *)buffer, 0, MSG_DONTWAIT, ( struct sockaddr *) &cliaddr, &len);
    if(n != 0) {
        std::cout << n << std::endl;
        return n;
    }else{
        std::cerr << "Empty buffer" << std::endl;
        return -1;
    }
}