//
// Created by martin on 17/07/2020.
//

#include <arpa/inet.h>
#include <netdb.h>
#include <zconf.h>
#include "Transceiver.h"

Transceiver::Transceiver(int port) {
    this->port = port;
}

int Transceiver::init() {

    if ((this->sockServer = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
        this->errorString = "socket creation failed: " + std::string(strerror(errno));
        this->logError();
        return -1;
    }

    memset(&this->servaddr, 0, sizeof(this->servaddr));

    this->servaddr.sin_family = AF_INET; // IPv4
    this->servaddr.sin_addr.s_addr = INADDR_ANY;
    this->servaddr.sin_port = htons(this->port);

    if (bind(this->sockServer, (const struct sockaddr *)&this->servaddr, sizeof(this->servaddr)) < 0 ) {
        this->errorString = "bind failed: " + std::string(strerror(errno));
        this->logError();
        return -1;
    }

    return 0;
}

void Transceiver::logError() {
    std::cerr << this->errorString << std::endl;
}

Request Transceiver::checkSocket() {
    Request request;
    struct sockaddr_in cliaddr;
    memset(&cliaddr, 0, sizeof(cliaddr));

    unsigned int len =0;
    ssize_t n = 0;
    char buffer[1024];
    len = sizeof(cliaddr);

    n = recvfrom(this->sockServer, &buffer, 1024, MSG_DONTWAIT, ( struct sockaddr *) &cliaddr, &len);

    if(n > 0) {
        buffer[n] = '\0';
        request.message = std::string(buffer);
        request.ip = std::string(inet_ntoa(cliaddr.sin_addr));
        request.valid = true;
    } else {
        request.valid = false;
    }
    return request;
}

bool Transceiver::isValid(Request *req) {
    if(req->message.find(':') != std::string::npos){
        std::size_t pos = req->message.find(':');
        int port = stoi(req->message.substr(pos+1));
        if(port < 65535 && port > 0){
            req->port = port;
            return true;
        }
    }
    return false;
}

bool Transceiver::sendResponse(Response res) {
    if(sockClient == -1) {
        std::cout << "First sockClient creation" << std::endl;
        if ((sockClient = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
            this->errorString = "Client socket creation failed: " + std::string(strerror(errno));
            this->logError();
            return false;
        }
    }

    struct sockaddr_in clientaddr;

    memset(&clientaddr, 0, sizeof(clientaddr));

    clientaddr.sin_family = AF_INET; // IPv4
    clientaddr.sin_addr.s_addr = inet_addr(res.ip.c_str());
    clientaddr.sin_port = htons(9020);

    if ( sendto(sockClient, res.message.c_str(), res.message.length(), 0, (const sockaddr*)&clientaddr, sizeof(clientaddr)) < 0 ) {
        this->errorString = "Client socket sendto failed: " + std::string(strerror(errno));
        this->logError();
        return false;
    }

    return true;
}

std::vector<std::pair<std::string, std::string>> Transceiver::getAllNetworkInterfacesAddress() {
    struct ifaddrs *ifaddr, *ifa;
    int family, s;
    std::vector<std::pair<std::string, std::string>> result;

    if (getifaddrs(&ifaddr) == -1) {
        this->errorString = "GetIfAddrs failed: "+std::string(strerror(errno));
        this->logError();
    } else {
        for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
            family = ifa->ifa_addr->sa_family;
            if (family == AF_INET) {
                char addr[NI_MAXHOST];
                char netmask[NI_MAXHOST];
                s = getnameinfo(ifa->ifa_addr,
                                (family == AF_INET) ? sizeof(struct sockaddr_in) :
                                sizeof(struct sockaddr_in6),
                                addr, NI_MAXHOST,
                                NULL, 0, NI_NUMERICHOST);
                if(s != 0) {
                    this->errorString = "GetnameInfo() failed";
                    this->logError();
                } else {
                    s = getnameinfo(ifa->ifa_netmask,
                                    (family == AF_INET) ? sizeof(struct sockaddr_in) :
                                    sizeof(struct sockaddr_in6),
                                    netmask, NI_MAXHOST,
                                    NULL, 0, NI_NUMERICHOST);
                    if(s != 0) {
                        this->errorString = "GetnameInfo() failed";
                        this->logError();
                    } else {
                        result.push_back(std::make_pair<std::string, std::string>(addr, netmask));
                    }
                }
            }
        }
    }
    freeifaddrs(ifaddr);
    return result;
}

std::vector<std::string>
Transceiver::calculateBroadcastAddress(std::vector<std::pair<std::string, std::string>> addrNetmaskList) {
    std::vector<std::string> result;
    struct in_addr host, mask, broadcast;

    for(auto it = addrNetmaskList.begin(); it != addrNetmaskList.end(); ++it) {
        char broadcast_address[INET_ADDRSTRLEN];

        inet_pton(AF_INET, it->first.c_str(), &host);
        inet_pton(AF_INET, it->second.c_str(), &mask);
        broadcast.s_addr = host.s_addr | ~mask.s_addr;
        inet_ntop(AF_INET, &broadcast, broadcast_address, INET_ADDRSTRLEN);
        result.emplace_back(broadcast_address);
    }

    return result;
}

bool Transceiver::sendRequest(Request req) {
    int broadcast = 1;
    if(sockClient == -1) {
        if ((sockClient = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
            this->errorString = "Client socket creation failed: " + std::string(strerror(errno));
            this->logError();
            return false;
        }
    }

    if(setsockopt(sockClient,SOL_SOCKET,SO_BROADCAST,&broadcast,sizeof(broadcast)) < 0) {
        this->errorString = "Client socket setting Broadcast option: " + std::string(strerror(errno));
        this->logError();
        return false;
    }

    struct sockaddr_in clientaddr;

    memset(&clientaddr, 0, sizeof(clientaddr));

    clientaddr.sin_family = AF_INET; // IPv4
    clientaddr.sin_addr.s_addr = inet_addr(req.ip.c_str());
    clientaddr.sin_port = htons(req.port);

    if ( sendto(sockClient, req.message.c_str(), req.message.length(), 0, (const sockaddr*)&clientaddr, sizeof(clientaddr)) < 0 ) {

        this->errorString = "Client socket sendto failed: " + std::string(strerror(errno));
        this->logError();
        return false;
    }
    return true;
}
