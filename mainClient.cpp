#include <iostream>
#include <systemd/sd-daemon.h>
#include <csignal>
#include <atomic>
#include <chrono>

#include "InterruptableSleeper.cpp"
#include "Transceiver.h"
#include "Monitor.dto.h"
#include "OledFont8x16.h"
#include "OledI2C.h"


std::atomic_bool running;
InterruptableSleeper sleeper;

const int _ASK_PORT = 9020;
const int _REQ_PORT = 9019;
const std::string _I2C_DEVICE = "/dev/i2c-1";
const uint8_t _I2C_ADDRESS = 0x3C;

bool firstMsgReceived = false;

SSD1306::OledI2C *oled;

void signal_handler(int signal) {
    running = false;
    sleeper.wake();
}

Request prepareAskRequest(std::string address) {
    Request req;
    req.message = "ASK:"+std::to_string(_ASK_PORT);
    req.ip = address;
    req.port = _REQ_PORT;
    return req;
}

void printAllInterfacesAddresses(std::vector<std::pair<std::string, std::string>> &interfacesInformations) {
    oled->clear();
    for(int i = 0; i < interfacesInformations.size(); i++) {
        drawString8x12(SSD1306::OledPoint{0, i * 12},
                       interfacesInformations[i].first.c_str(),
                       SSD1306::PixelStyle::Set,
                       *oled);
    }
    oled->displayUpdate();
}

int main(int argc, char** argv) {
    std::cout << "Starting..." << std::endl;
    running = true;
    std::signal(SIGTERM, signal_handler);

    oled = new SSD1306::OledI2C(_I2C_DEVICE, _I2C_ADDRESS);
    Transceiver transceiver(_ASK_PORT);
    transceiver.init();

    while (running) {
        sd_notify(0, "WATCHDOG=1");
        auto interfacesAddresses = transceiver.getAllNetworkInterfacesAddress();
        auto broadcastAddressList = transceiver.calculateBroadcastAddress(interfacesAddresses);
        for(auto it = broadcastAddressList.begin(); it != broadcastAddressList.end(); ++it) {
            Request req = prepareAskRequest(*it);
            transceiver.sendRequest(req);
        }

        if(!firstMsgReceived) {
            printAllInterfacesAddresses(interfacesAddresses);
        }

        auto res = transceiver.checkSocket();
        if(res.valid) {
            MonitorDto dto = decomposeMessage(res.message);
            dto.displayDto(*oled);
            firstMsgReceived = true;
        }
        sleeper.sleepFor(std::chrono::milliseconds(1000));
    };

    std::cout << "Stopping..." << std::endl;

    return (EXIT_SUCCESS);
}
