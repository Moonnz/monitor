//
// Created by martin on 27/07/2020.
//

#ifndef MONITOR_MONITOR_DTO_H
#define MONITOR_MONITOR_DTO_H

#include <string>
#include <vector>
#include <sstream>
#include <iomanip>
#include "OledI2C.h"
#include "OledFont8x12.h"

const double megabyte = 1024 * 1024;

float KbToGb(int Kb) {
    return (float)Kb / megabyte;
}

struct MonitorDto {
    int usedRam;
    int availableRam;
    int totalRam;
    std::string uptime;
    std::string cpuTemp;
    std::string fanSpeed;
    std::string cpuUsage;
    void displayDto(SSD1306::OledI2C& oled) {
        std::stringstream stream;
        stream << std::fixed << std::setprecision(1) << KbToGb(usedRam);
        stream << '/' << KbToGb(totalRam);
        oled.clear();
        drawString8x12(SSD1306::OledPoint{0, 0 * 12},
                       uptime,
                       SSD1306::PixelStyle::Set,
                       oled);
        drawString8x12(SSD1306::OledPoint{0, 1 * 12},
                      std::string("Cpu: ")+cpuTemp+std::string("C ")+cpuUsage.substr(0, 4)+std::string("%"),
                      SSD1306::PixelStyle::Set,
                      oled);
        drawString8x12(SSD1306::OledPoint{0, 2 * 12},
                       std::string("Fan: ")+fanSpeed+std::string(" RPM"),
                       SSD1306::PixelStyle::Set,
                       oled);
        drawString8x12(SSD1306::OledPoint{0, 3 * 12},
                       std::string("Ram: ")+stream.str()+std::string(" GB"),
                       SSD1306::PixelStyle::Set,
                       oled);
        oled.displayUpdate();
    }
};

std::vector<std::string> split(const std::string& str, const std::string& delim) {
    std::vector<std::string> tokens;
    size_t prev = 0, pos = 0;
    do
    {
        pos = str.find(delim, prev);
        if (pos == std::string::npos) pos = str.length();
        std::string token = str.substr(prev, pos-prev);
        if (!token.empty()) tokens.push_back(token);
        prev = pos + delim.length();
    }
    while (pos < str.length() && prev < str.length());
    return tokens;
}

MonitorDto decomposeMessage(std::string str) {
    MonitorDto dto;
    std::vector<std::string> decomposedStr = split(str, "::");
    if(decomposedStr.size() != 7) {
        return dto;
    }
    dto.usedRam = std::stoi (decomposedStr[0]);
    dto.availableRam = std::stoi (decomposedStr[1]);
    dto.totalRam = std::stoi (decomposedStr[2]);
    dto.uptime = decomposedStr[3];
    dto.cpuTemp = decomposedStr[4];
    dto.fanSpeed = decomposedStr[5];
    dto.cpuUsage = decomposedStr[6];
    return dto;
}


#endif //MONITOR_MONITOR_DTO_H
