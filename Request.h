//
// Created by martin on 17/07/2020.
//

#ifndef MONITOR_SERVER_REQUEST_H
#define MONITOR_SERVER_REQUEST_H

#include <string>

struct Request
{
    std::string message;
    std::string ip;
    int port;
    bool valid;
};

#endif //MONITOR_SERVER_REQUEST_H
