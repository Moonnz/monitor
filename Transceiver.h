//
// Created by martin on 17/07/2020.
//

#ifndef MONITOR_SERVER_TRANSCEIVER_H
#define MONITOR_SERVER_TRANSCEIVER_H


#include <netinet/in.h>
#include <sys/socket.h>
#include <ifaddrs.h>
#include <string>
#include <iostream>
#include <cstring>
#include <vector>

#include "Request.h"
#include "Response.h"

class Transceiver {
public:
    Transceiver(int port = 9019);
    int init();
    Request checkSocket();
    bool isValid(Request*);

    bool sendRequest(Request);
    bool sendResponse(Response);
    std::vector<std::pair<std::string, std::string>> getAllNetworkInterfacesAddress();
    std::vector<std::string> calculateBroadcastAddress(std::vector<std::pair<std::string, std::string>>);

private:
    int sockServer;
    int sockClient = -1;

    struct sockaddr_in servaddr;

    int port;
    std::string errorString;

    void logError();
};


#endif //MONITOR_SERVER_TRANSCEIVER_H
