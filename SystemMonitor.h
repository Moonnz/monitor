//
// Created by martin on 22/07/2020.
//

#ifndef MONITOR_SYSTEMMONITOR_H
#define MONITOR_SYSTEMMONITOR_H

#include <linux/kernel.h>
#include <sys/sysinfo.h>
#include <linux/types.h>
#include <tuple>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <thread>

#include "types.h"
#include "Response.h"

class SystemMonitor {
public:
    std::tuple<int, int, int> getRam();
    std::string getUptime();
    std::vector<std::pair<std::string, std::string>> getCPUUsage();
    int getCPUTemp();
    int getFanSpeed();
    std::string getRecap();

private:
    const long minute = 60;
    const long hour = minute * 60;
    const long day = hour * 24;
    const double megabyte = 1024 * 1024;
    void readStatsCPU(std::vector<CPUData> & entries);
    size_t GetIdleTime(const CPUData & e);
    size_t GetActiveTime(const CPUData & e);
    std::vector<std::string> split(const std::string& str, const std::string& delim);
};

#endif //MONITOR_SYSTEMMONITOR_H
